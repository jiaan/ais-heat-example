/*
	Name: 		AIS_fnc_hitPart
	
	Desc:		HEAT warhead(s) simulation for vehicles
	
	Param(s):	standard hitPart output
	
	Returns:	nil
	
	Author:		Jiaan "Bakerman"
	
	Example:	_this call AIS_fnc_hitPart
*/

// Exit if HEAT simulation is disabled
if !(AIS_HEAT_ENABLED) exitWith {};

// First hitPart element
params ["_hit"];

// Exit on non-direct hits
if !(_hit select 10) exitWith {};

// Get AIS CE penetrators and exit if none or empty
private _penenetrators = getArray(configFile >> "CfgAmmo" >> (_hit select 6 select 4) >> "ais_ce_penetrators");
if (_penenetrators isEqualTo []) exitWith {};

// Vehicle
private _vehicle = _hit select 0;

// Exit with simple simulation for non tanks
if !(_vehicle isKindOf "tank") exitWith {
	// Projectile velocity
	private _velocity = (vectorNormalized (_hit select 4)) vectorMultiply 1000;
	private _position = ASLToAGL (_hit select 3);
	// Spawn projectiles sequentially
	{
		private _projectile = createVehicle [_x, _position, [], 0, "CAN_COLLIDE"];
		_projectile setVelocity _velocity;
		/*
		private _ball = createVehicle ["Sign_Sphere10cm_F", _position, [], 0, "CAN_COLLIDE"];
		_ball setObjectTexture [0, '#(argb,8,8,3)color(0.5,'+(_forEachIndex / 3)+',0.0,0.1,ca)'];
		systemChat str ['simple', _x, _forEachIndex, diag_frameno];
		*/
	} forEach _penenetrators;
};

// Projectile velocity
private _velocity = vectorNormalized (_hit select 4);

// Move penetrators back 0.2 meter and convert to AGL
private _position = ASLToAGL ((_hit select 3) vectorAdd (_velocity vectorMultiply -0.2));

// Legacy CE surface modifier for RHS vehicles
private _modifier = RHS_hitPartModifier find (_hit select 9);

// Transform vector into velocity and apply modifier
if (_modifier != -1) then {
	_velocity = _velocity vectorMultiply (1000 * ([0.95,0.9,0.85,0.8,0.75,0.7,0.65,0.6,0.5,0.4,0.3,0.2,0.1] select _modifier));
} else {
	_velocity = _velocity vectorMultiply 1000;
};

// Vehicle CE animation
private _animation = getText(configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "ais_ce_protector");

// Do protector animation if available and not deployed
private _revert = false;
if (_animation != "") then {
	if ((_vehicle animationPhase _animation) != 1) then {
		_revert = true;
		_vehicle animate [_animation, 1];
	};
};

// Create the first projectile
private _projectile = createVehicle [_penenetrators select 0, _position, [], 0, "CAN_COLLIDE"];
_projectile setVelocity _velocity;

/*
private _ball = createVehicle ["Sign_Sphere10cm_F", _position, [], 0, "CAN_COLLIDE"];
_ball setObjectTexture [0, '#(argb,8,8,3)color(0.0,0.0,0.5,0.1,ca)'];
systemChat str [_penenetrators select 0, 0, diag_frameno];
*/

// Exit on single warhead
private _count = count _penenetrators;
if (_count == 1) exitWith {
	// Revert animation?
	if (_revert) then {
		// Spawn to wait until penetrator passes
		[_animation, _vehicle, diag_frameno] spawn {
			params ["_animation","_vehicle","_spawnFrame"];
			waitUntil {
				(diag_frameno > _spawnFrame + 1);
			};
			_vehicle animate [_animation, 0];
		};
	};
};

// Do onEachFrame event for multiple warheads
private _id = "RHS_pfh_hitPart_" + str _projectile;
[_id, "onEachFrame", {
	params ["_array","_index","_count","_velocity","_position","_lastFrame","_revert","_animation","_vehicle"];
	// Run on next frame
	if (diag_frameno > _lastFrame) then {
		// If index is in array bounds
		if (_index < _count) then {
			// Update frame number
			_this set [5, diag_frameno];
			// If the vehicle has a CE animation
			if (_animation != "") then {
				// Check state, if not animated do animation
				if ((_vehicle animationPhase _animation) != 1) then {
					// Update revert status
					if (!_revert) then {
						_this set [6, true];
					};
					// Do CE animation
					_vehicle animate [_animation, 1];
				};
			};
			// Create projectile from array
			private _projectile = createVehicle [_array select _index, _position, [], 0, "CAN_COLLIDE"];
			_projectile setVelocity _velocity;
			/*
			private _ball = createVehicle ["Sign_Sphere10cm_F", _position, [], 0, "CAN_COLLIDE"];
			_ball setObjectTexture [0, '#(argb,8,8,3)color(0.5,0.0,0.0,0.1,ca)'];
			systemChat str [_array select _index, _index, diag_frameno, _lastFrame, _revert];
			*/
			// Update index
			_this set [1, _index + 1];
		} else {
			// Revert animation and exit EH
			if (_revert) then {
				// Wait one frame
				if (diag_frameno > _lastFrame + 1) then {
					// Check state and do animation if required
					if ((_vehicle animationPhase _animation) != 0) then {
						_vehicle animate [_animation, 0];
					};
					[_this select 9, "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
				} else {
					// Check state and do animation if required
					if ((_vehicle animationPhase _animation) != 1) then {
						_vehicle animate [_animation, 1];
					};
				};
			} else {
				[_this select 9, "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
			};
		};
	};
}, [_penenetrators, 1, _count, _velocity, _position, diag_frameno, _revert, _animation, _vehicle, _id]] call BIS_fnc_addStackedEventHandler;