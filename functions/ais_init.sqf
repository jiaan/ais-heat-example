// Disable all other HEAT simulations
AIS_PENETRATION_ENABLED = false;

// Enable AIS HEAT simulation
if (isNil "AIS_HEAT_ENABLED") then {
	AIS_HEAT_ENABLED = true;
};

// Legacy CE modifier
RHS_hitPartModifier = [
	"rhsafrf\addons\rhs_main\penetration\ce_0.95.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.90.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.85.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.80.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.75.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.70.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.65.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.60.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.50.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.40.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.30.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.20.bisurf",
	"rhsafrf\addons\rhs_main\penetration\ce_0.10.bisurf"
];