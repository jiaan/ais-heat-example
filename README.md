# AIS HEAT Simulation Example

This is an example script for simulation [HEAT](https://en.wikipedia.org/wiki/High-explosive_anti-tank_warhead) ammunition in ARMA 3 as used in [RHS](http://www.rhsmods.org/). In standard ARMA an explosive can either be purely KE (kinetic) or CE (chemical / explosive) based, but not both. This script simulates HEAT by configuring certain CE rounds to spawn a series of KE penetrators.


