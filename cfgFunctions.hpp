
class CfgFunctions
{
	class AIS
	{
		tag = "AIS";
		class functions
		{
			class init
			{
				file = ".......\functions\rhs_init.sqf";
				description = "Initialization";
				preInit = 1;
			};
			class hitPart
			{
				file = ".......\functions\rhs_hitPart.sqf";
				description = "Advanced CE penetration simulation for warheads and vehicles";
			};
		};
	};
};
