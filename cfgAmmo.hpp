//RHA calibration macro
#define RHA(mm,speed) caliber=(mm/((15*speed)/1000));

class CfgAmmo
{
	class Default;
	class BulletCore;
	class BulletBase;
	
	// Base class for AIS CE penetrators
	class ais_penetrator_ce: BulletBase
	{
		typicalSpeed = 1000; // Slug speed
		deflecting = 11; // Typical deflection angle for HEAT
		timeToLive = 0.010; // 10 meter max potential life, in practice this goes down to 5m
		simulationStep = 0.001;
		airFriction = -0.1;
		ais_penetrator_type = 1; // CE
		caliber = 0;
		hit = 300;
		indirectHit = 10;
		indirectHitRange = 0.1;
		model="\A3\Weapons_f\Data\bullettracer\tracer_yellow";
		class HitEffects
		{
			hitMetal="ImpactMetalSabotBig";
			hitMetalPlate="ImpactMetalSabotBig";
			hitBuilding="ImpactConcreteSabot";
			hitConcrete="ImpactConcreteSabot";
			hitGroundSoft="ImpactEffectsGroundSabot";
			hitGroundHard="ImpactEffectsGroundSabot";
			default_mat="ImpactEffectsGroundSabot";
		};
	};
	// Base class for AIS tandem warhead CE penetrators
	class ais_penetrator_ce_tandem: ais_penetrator_ce
	{
		caliber = 3;
		hit = 40;
	};
	
	// My mod weapons start here
	class mod_rpg : mod_rpg_base
	{
		// Specify which penetrators to spawn on impact
		ais_ce_penetrators[] = { "mod_rpg_penetrator" };
		// This is the hit for HE component ONLY
		hit = 40;
		indirectHit = 7;
		indirectHitRange = 2.6;
		caliber=0.1;
	};
	class mod_rpg_penetrator: ais_penetrator_ce
	{
		// This warhead can penetrate 400mm RHA
		RHA(400, 1000)
	};
	
};






