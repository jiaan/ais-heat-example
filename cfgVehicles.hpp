
class DefaultEventhandlers {
	class AIS_DefaultEventhandlers
	{
		hitpart = "_this call ais_fnc_hitPart";
	};
};

class CfgVehicles 
{
	class All;
	class AllVehicles;
	class Air;
	class Helicopter;
	class Plane;
	class Land;
	class Man;
	class CAManBase;
	class LandVehicle;
	class Tank;
	class Tank_F;
	class Car;
	class Car_F;
	class Ship;
	class Ship_F;
};
